# OpenML dataset: 1M-python-questions-on-stackoverflow

https://www.openml.org/d/43662

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Just made a scraper for stackoverflow, and created a dataset. Hope it will be useful for your task
Content
Contains 1 csv file, containing following columns
question_vote_count :    Number of votes of that question

question_answer_count :  Answer counts of that question

question_title :  Title of question

question_title_link :  Url link of that question

question_tags : Tags of that question

Acknowledgements
Thanks to great platform Stackoverflow
Inspiration
Whats your idea with this data?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43662) of an [OpenML dataset](https://www.openml.org/d/43662). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43662/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43662/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43662/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

